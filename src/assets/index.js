import game_bg from './images/game_bg.png';
import game_cannon_base from './images/game_cannon_base.png';
import game_cannon_body from './images/game_cannon_body.png';
import game_cannon_bullet from './images/game_cannon_bullet.png';
import game_monster_b from './images/game_monster_b.png';
import game_monster_s from './images/game_monster_s.png';


export default {
    img: [
        {name: 'game_bg', res: game_bg},
        {name: 'game_cannon_base', res: game_cannon_base},
        {name: 'game_cannon_body', res: game_cannon_body},
        {name: 'game_cannon_bullet', res: game_cannon_bullet},
        {name: 'game_monster_b', res: game_monster_b},
        {name: 'game_monster_s', res: game_monster_s},
    ]
}
