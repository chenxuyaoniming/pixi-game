import * as Pixi from 'pixi.js';
import Game from "./plugin/Game";
import ticker from "./plugin/ticker";

const instance = Game();
document.body.append(instance.scene.view)

const draw = new Pixi.Graphics();
draw.beginFill(0x9966FF);
draw.drawEllipse(0, 0, 500, 200);
draw.endFill();
draw.x = 500;
draw.y = 300;


instance.scene.stage.addChild(draw);

function createSelf(radio) {
    const f = new Pixi.Graphics();
    f.beginFill(0x33FF00);
    f.drawRoundedRect(-50, -50, 100, 100, 20);
    f.endFill();
    f.x = 500 + Math.cos(radio)*500;
    f.y = 300 + Math.sin(radio)*200;
    const run = () => {
        radio += 0.001;
        let x = 500 + Math.cos(radio)*500;
        let y = 300 + Math.sin(radio)*200;
        f.x = x;
        f.y = y;
    }
    ticker.add(run);
    instance.scene.stage.addChild(f);
}

createSelf(0);
createSelf(-1.5);
createSelf(1.5);
createSelf(3.2);









