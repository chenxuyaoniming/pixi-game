import * as Pixi from 'pixi.js';
import loader from './components/loader';
import asset from './assets';
import Game from "./components/Game";
import Sprite from "./components/Sprite";
import Connon from "./components/Cannon";
import Vconsole from 'vconsole';
import Monster from "./components/Monster";
import Text from './components/Text';

new Vconsole();

const instance = Game();

function showStage() {
    instance.scene.view.style.position = 'fixed';
    instance.scene.view.style.left = '0';
    instance.scene.view.style.top = '0';
    document.body.append(instance.scene.view);
}

function createBg() {
    const bg = new Sprite('game_bg');
    bg.width = window.innerWidth;
    bg.height = window.innerHeight;

    instance.scene.stage.addChild(bg);
}

function main(...props) {
    showStage();
    createBg();
    new Connon();
    new Text();
    setInterval(() => {
        new Monster();
    }, 3000);
}

loader({
    resources: asset.img,
    cb: main,
    instance
});
