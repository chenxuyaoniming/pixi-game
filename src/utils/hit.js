
export const bulletHit = (m, b) => {
    const m_left = m.x;
    const m_right = m.x + m.width;
    const m_top = m.y;
    const m_bottom = m.y + m.height;

    return m_left < b.x && m_right > b.x && m_top < b.y && m_bottom > b.y;
}
