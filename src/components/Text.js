import * as Pixi from 'pixi.js';
import Game from "./Game";

class Text {
    instance = Game();

    text = ''

    constructor(options = {}) {
        console.log(this.instance)
        this.text = new Pixi.Text(`总分：${this.instance.state.reword}`, {
            fontFamily: "Arial",
            fontSize: 48,
            fill: "white",
            stroke: '#ff3300',
            strokeThickness: 4,
            dropShadow: true,
            dropShadowColor: "#000000",
            dropShadowBlur: 4,
            dropShadowAngle: Math.PI / 6,
            dropShadowDistance: 6,
            ...options
        });
        this.instance.msg = this;
        this.text.x = 20;
        this.text.y = 20;
        this.instance.scene.stage.addChild(this.text);
    }
    update() {
        this.text.text = `总分：${this.instance.state.reword}`;
    }
}

export default Text;
