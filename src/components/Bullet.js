import Game from "./Game";
import Sprite from "./Sprite";
import ticker from "./ticker";
import { SCALE } from '../constant'



class Bullet {
	instance = Game();

	constructor(props) {
		const { rotation, y } = props;
		this.bullet = new Sprite('game_cannon_bullet');
		this.bullet.scale.set(SCALE, SCALE);
		this.bullet.anchor.x = 0.5;
		this.bullet.anchor.y = 0.5;
		this.bullet.rotation = rotation;
		this.bullet.x = window.innerWidth / 2;
		this.bullet.y = y;
		this.bullet.zIndex = -1;

		this.instance.scene.stage.addChild(this.bullet);
		this.instance.scene.stage.setChildIndex(this.bullet, 2);
		this.instance.bullets.push(this.bullet);

		this.run(rotation);
	}

	run(r) {
		let stepX = r;
		let stepY = -1;
		const move = () => {
			stepX += r;
			stepY += -1;
			this.bullet.x += stepX;
			this.bullet.y += stepY;

			if (this.bullet.x < -this.bullet.width || this.bullet.y < -this.bullet.height) {
				ticker.remove(move);
			}
		}

		const remove = () => {
			ticker.remove(move);
			this.instance.scene.stage.removeChild(this.bullet);
		}
		this.bullet.remove = remove;
		ticker.add(move);
	}
}


export default Bullet;