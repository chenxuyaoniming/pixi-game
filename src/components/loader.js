import * as Pixi from 'pixi.js';


export default function loader(props) {
    const { resources, instance, cb } = props;
    let loadingCount = 0;

    if (typeof resources === 'string') {
        throw Error('resources must be Array');
    }
    if (typeof resources === 'object') {
        resources.reduce((acc, item) => acc.add(item.name, item.res), instance.scene.loader).load();
    }
    // @ts-ignore
    instance.scene.loader.onProgress.add((...prop) => {
        loadingCount += 1;
        console.log(`进度: ${loadingCount}/${resources.length}`)
    })
    // @ts-ignore
    instance.scene.loader.onComplete.add(function (target) {
        cb();
    })

}
