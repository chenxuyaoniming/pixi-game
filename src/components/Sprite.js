import * as Pixi from 'pixi.js';
import Game from "./Game";

class Sprite {

    game = Game();

    constructor(name) {
        return new Pixi.Sprite(this.game.scene.loader.resources[name].texture)
    }
}

export default Sprite;
