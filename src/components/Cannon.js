import Game from "./Game";
import Sprite from "./Sprite";
import ticker from "./ticker";
import { SCALE } from '../constant';
import Bullet from "./Bullet";


class Connon {
	instance = Game()
	anchorPos = {};
	lastRotate = 0;
	safeHeight = 0;

	constructor() {
		this.cannonBase = new Sprite('game_cannon_base');
    this.cannonBody = new Sprite('game_cannon_body');

    this.cannonBase.scale.set(SCALE, SCALE);
    this.cannonBody.scale.set(SCALE, SCALE);

    this.cannonBase.x = (window.innerWidth / 2) - (this.cannonBase.width / 2);
    this.cannonBase.y = window.innerHeight - this.cannonBase.height/2;

    this.cannonBody.anchor.x = 0.5;
    this.cannonBody.anchor.y = 0.8;

		this.anchorPos = {
			x: window.innerWidth / 2,
			y: window.innerHeight - this.cannonBody.height * .2
		}

    this.cannonBody.x = this.anchorPos.x;
    this.cannonBody.y = this.anchorPos.y;

		console.log(this.cannonBody)

		this.safeHeight = window.innerHeight - this.cannonBody.height*3/2;


    this.instance.scene.stage.addChild(this.cannonBase);
    this.instance.scene.stage.addChild(this.cannonBody);

		this.listener();
	}

	listener() {
		window.addEventListener('touchstart', this.touchEvent.bind(this))
	}

	touchEvent(e) {
		const { clientX, clientY } = e.changedTouches[0];
		if (clientY > this.safeHeight) {
			return;
		}
		const x = this.anchorPos.x - clientX;
		const y = this.anchorPos.y - clientY;

		const rotate = this.cannonBody.rotation ? (+this.cannonBody.rotation).toFixed(2) : 0;
		let nextRotate = -(x/y).toFixed(2);
		if (this.lastRotate == nextRotate) {
			return;
		}
		this.lastRotate = nextRotate;

		let step;
		if (rotate >= 0) {
			step = rotate > nextRotate ? -0.01 : 0.01;
		} else {
			step = rotate > nextRotate ? -0.01 : 0.01;
		}


		const run = () => {
			const { rotation = 0 } = this.cannonBody;
			const rotationStep = +(+rotation + step).toFixed(2);
			if (rotationStep ==  nextRotate.toFixed(2) || Math.abs(rotationStep) >= 1.2) {
				ticker.remove(run);
				ticker.addOnce(() => {
					new Bullet({
						rotation: nextRotate,
						y: this.anchorPos.y,
					})}
				);
				return;
			}
			this.cannonBody.rotation = rotationStep;
		}

		ticker.remove(run);
		ticker.add(run);

	}

}



export default Connon;

