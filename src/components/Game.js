import * as Pixi from 'pixi.js';

class Game {

    stageWitdh = window.innerWidth

    stageHeight = window.innerHeight

    state = {
        reword: 0,
        play: false,
        over: false,
        paused: false,
    }

    msg = null;

    bullets = [];

    scene = new Pixi.Application({
        width: this.stageWitdh,
        height: this.stageHeight,
        transparent: true,
        backgroundColor: 0x00000,
    })

    rerender() {
        this.scene.view.width = this.window.innerWidth;
        this.scene.view.height = this.window.innerHeight;
    }
}


function createGame() {
    let game;
    return () => {
        if (!game) {
            game = new Game();
        }
        return game;
    }
}
export default createGame();


