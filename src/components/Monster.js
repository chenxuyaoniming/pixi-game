import * as Pixi from 'pixi.js';
import ticker from "./ticker";
import { bulletHit } from "../utils/hit";
import Game from "./Game";
import Sprite from "./Sprite";
import { SCALE } from '../constant';
class Monster{

    instance = Game()

    speed = 0

    monsterNames = [
        {name: 'game_monster_b', hit: 2, scale: 1.8, reword: 2},
        {name: 'game_monster_s', hit: 1, scale: 1, reword: 1}
    ]

    hitCount = 0

    alphaStep = 0.1;

    constructor(props) {
        this.speed = 6;

        this.init();
    }

    init() {
        const randomIndex =Math.floor( Math.random() * 1 + 0.4);
        const info = this.monsterNames[randomIndex];
        this.ms = new Sprite(info.name);
        this.ms.info = info;
        this.ms.scale.set(SCALE*info.scale);
        this.ms.x = Math.random() * (window.innerWidth - this.ms.width);
        this.ms.y = Math.random() * 200 - 400;
        // @ts-ignore
        this.instance.scene.stage.addChild(this.ms);
        this.run();
    }

    run() {
        const move = () => {
            this.ms['y'] += this.speed;
            if (this.instance.bullets.length) {
                for(let i = 0; i < this.instance.bullets.length; i++) {
                    const isHit = bulletHit(this.ms, this.instance.bullets[i]);
                    if (isHit) {
                        this.hitCount += 1;
                        this.instance.bullets[i].remove();
                        this.instance.bullets.splice(i, 1);
                        i --;
                        if (this.hitCount >= this.ms.info.hit) {
                            ticker.remove(move);
                            this.destory();
                            return;
                        }
                    }
                }
            }
            if (this.hitCount) {
                this.ms.alpha = 0.8;
                this.ms.scale.set(SCALE);
            }
            if (this.ms.y >= window.innerHeight) {
                ticker.remove(move);
                // @ts-ignore
                this.instance.scene.stage.removeChild(this.ms);
            }
        }
        ticker.add(move);
    }
    destory() {
        this.instance.state.reword += this.ms.info.reword;
        this.instance.msg.update();
        this.instance.scene.stage.removeChild(this.ms);
    }
}


export default Monster;
