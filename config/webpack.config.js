const path = require('path');
const Html = require('html-webpack-plugin');
const Clean = require('clean-webpack-plugin');
const Bar = require('webpackbar');

module.exports = {
    mode: "development",
    entry: {
        pixi: 'pixi.js',
        index: path.resolve(process.cwd(), 'src/index.js'),
    },
    output: {
        path: path.resolve(process.cwd(), 'dist'),
        filename: "js/[name].[chunkhash].js",
        chunkFilename: "js/[name].[chunkhash].js",
    },
    module: {
        rules: [
            {
                test: /.ts$/,
                use: "ts-loader",
                exclude: /node_modules/,
            },
            {
                test: /.(png|jpg|jpeg)$/,
                use: [
                    {
                        loader: "url-loader",
                        options: {
                            limit: 8192,
                            outputPath: 'assets',
                            filename: `[name].[ext]`
                        }
                    }
                ],
            }
        ]
    },
    plugins: [
        new Html({
            template: path.resolve(process.cwd(), 'public/index.html'),
            filename: "index.html",
        }),
        new Clean.CleanWebpackPlugin(),
        new Bar(),
    ],
    resolve: {
        extensions: ['.ts', '.js']
    },
    devServer: {
        hot: true,
        port: 3000,
    }
}
